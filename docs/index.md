# 🏡 Accueil

Méthodes avancées en algorithmique avec beaucoup de mathématiques.

- Pour tout ce qui est hors programme NSI

![logo Python](./images/logo-python.svg){width=300}

Le cours utilise Python comme langage de programmation pour les exemples.


!!! info "Partie 1"
    Dans la [première partie](https://ens-fr.gitlab.io/algo1/){ target=_blank }, on découvre l'algorithmique et le langage Python, avec France-IOI (Niveau 1, 2 et moitié du 3) pour les exemples.

!!! info "Partie 2"
    Dans la [deuxième partie](https://ens-fr.gitlab.io/algo2/){ target=_blank }, on finit le niveau 3, et on aborde les niveaux 4 et 5 de France-IOI.

!!! done "Partie 3"
    Bienvenue dans ce cours avec de nombreux problèmes issus :
    
    - Des niveaux 4, 5 et 6 de France-IOI
    - Des problèmes de Prologin
    - Des problèmes de Project Euler
    - Et d'autres encore


---

!!! info "À propos de ce site - RGPD"
    - Aucun _cookie_ n'est créé.
    - Il n'y a aucun lien vers des pisteurs.
        - Pas de polices Google qui espionne.
        - Pas de CDN malicieux.
    - Le langage Python est émulé par [Basthon](https://basthon.fr/){ target=_blank } sur votre propre machine.
        - Vous n'avez besoin de rien installer.
        - Strictement aucun code, aucune donnée personnelle n'est envoyée.
    - Adapté pour les PC, les tablettes et aussi les téléphones. 
    - Très peu gourmand en ressources, les pages sont légères.
    - Code source du contenu sous licence libre, sans utilisation commerciale possible.
